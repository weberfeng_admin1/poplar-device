
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)

PRODUCT_NAME := poplar
PRODUCT_DEVICE := poplar
PRODUCT_BRAND := Android
PRODUCT_MODEL := poplar
PRODUCT_MANUFACTURER := linaro

DEVICE_PACKAGE_OVERLAYS := device/linaro/poplar/overlay
PRODUCT_CHARACTERISTICS := tablet

# automatically called
$(call inherit-product, device/linaro/poplar/device.mk)

