# poplar-device <a name="ZH-CN_TOPIC_0000001101286951"></a>

-   [简介](#section11660541593)
-   [搭建构建环境](#section161941989596)
-   [下载AOSP 源码](#section119744591305)
-   [下载poplar相关代码](#section169045116126)
-   [编译](#section169045116136)
-   [烧写和启动](#section1371113476307)

## 简介<a name="section11660541593"></a>

使用AOSP android-12.0.0_r7版本源码编译Hi3798CV200 poplar ROM

## 搭建构建环境<a name="section161941989596"></a>

安装所需的软件包 (Ubuntu 18.04)，需要64位版本的Ubuntu。

```
sudo apt-get install git-core gnupg flex bison build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 libncurses5 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig
```

## 下载AOSP源码<a name="section119744591305"></a>

下载 repo 工具:
```
mkdir ~/bin
PATH=~/bin:$PATH
curl https://mirrors.tuna.tsinghua.edu.cn/git/git-repo > ~/bin/repo
chmod a+x ~/bin/repo
```
初始化仓库:
```
repo init -u https://mirrors.tuna.tsinghua.edu.cn/git/AOSP/platform/manifest -b android-12.0.0_r7
```
同步源码树：
```
repo sync
```

## 下载poplar相关代码<a name="section169045116126"></a>

在AOSP android-12.0.0_r7 top 目录下：
```
rm -rf device/linaro (delete linaro directory)
git clone https://gitee.com/weberfeng_admin1/poplar-device.git  device/linaro -b android-12.0.0_r7
```

## 修改AOSP源码并编译<a name="section169045116136"></a>

1. 临时修改framework/base/keystore/java/android/security/KeyStore.java，解决system_server crash的问题，具体原因待查。

```
11-15 09:20:56.981  5462  5547 E AndroidRuntime: *** FATAL EXCEPTION IN SYSTEM PROCESS: LockSettingsService
11-15 09:20:56.981  5462  5547 E AndroidRuntime: java.lang.AssertionError: 4
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.security.KeyStore.state(KeyStore.java:68)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at com.android.server.locksettings.LockSettingsService.ensureProfileKeystoreUnlocked(LockSettingsService.java:742)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at com.android.server.locksettings.LockSettingsService.access$500(LockSettingsService.java:188)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at com.android.server.locksettings.LockSettingsService$1.run(LockSettingsService.java:760)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.os.Handler.handleCallback(Handler.java:938)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.os.Handler.dispatchMessage(Handler.java:99)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.os.Looper.loopOnce(Looper.java:201)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.os.Looper.loop(Looper.java:288)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at android.os.HandlerThread.run(HandlerThread.java:67)
11-15 09:20:56.981  5462  5547 E AndroidRuntime:        at com.android.server.ServiceThread.run(ServiceThread.java:44)

diff --git a/keystore/java/android/security/KeyStore.java b/keystore/java/android/security/KeyStore.java
index a9543443d3f4..2af10d8d7867 100644
--- a/keystore/java/android/security/KeyStore.java
+++ b/keystore/java/android/security/KeyStore.java
@@ -65,7 +65,8 @@ public class KeyStore {
             case UserState.LSKF_LOCKED:
                 return KeyStore.State.LOCKED;
             default:
-                throw new AssertionError(userState);
+                //throw new AssertionError(userState);
+                return KeyStore.State.UNLOCKED;
         }
     }
```

2. 编译

```
source build/envsetup.sh
lunch lunch poplar-eng
make -j8
```

## 烧写和启动<a name="section1371113476307"></a>

```
将boot.img、cache.img、system.img、userdata.img和vendor.img拷贝到device/linaro/poplar-tools目录下
运行命令：
./poplar_recovery_builder.sh all -a -u
cp -r recovery udisk_directory
cp recovery/fastboot.bin udisk_directory

使用U盘启动poplar：
usb reset
fatload usb 0:1 ${scriptaddr} recovery_files/install.scr
source ${scriptaddr}
```

启动截图：
![poplar-device](./android12_1.png)
![poplar-device](./android12_2.png)
![poplar-device](./android12_3.png)
